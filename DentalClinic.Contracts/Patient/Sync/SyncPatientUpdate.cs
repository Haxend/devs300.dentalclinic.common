﻿using DentalClinic.Common.Contracts.User;

namespace DentalClinic.Common.Contracts.Patient
{
    public class SyncPatientUpdate : UserDto
    {
        public Guid OuterId { get; set; }
        public string? Allergy { get; set; } = null!;
    }
}
