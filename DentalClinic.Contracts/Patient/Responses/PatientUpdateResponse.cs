﻿using DentalClinic.Common.Contracts.Common;

namespace DentalClinic.Common.Contracts.Patient;

public class PatientUpdateResponse : BaseResponse
{
    public Guid Id { get; set; }
}
