﻿using DentalClinic.Common.Contracts.Common;

namespace DentalClinic.Common.Contracts.Patient;

public class PatientCreateResponse : BaseResponse
{
    public Guid Id { get; set; }
}
