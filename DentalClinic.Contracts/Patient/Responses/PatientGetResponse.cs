﻿namespace DentalClinic.Common.Contracts.Patient;

public class PatientGetResponse : PatientDto
{
    public Guid Id { get; set; }
}
