﻿using DentalClinic.Common.Contracts.Common;

namespace DentalClinic.Common.Contracts.Patient;

public class PatientDeleteResponse : BaseResponse
{
    public Guid Id { get; set; }
}
