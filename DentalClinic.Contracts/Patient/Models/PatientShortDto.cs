﻿namespace DentalClinic.Common.Contracts.Patient;

public class PatientShortDto
{
    public Guid Id { get; set; }
    public string Fio { get; set; }
}
