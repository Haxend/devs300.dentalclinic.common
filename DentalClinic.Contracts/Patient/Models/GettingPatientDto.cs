﻿using DentalClinic.Common.Contracts.User;

namespace DentalClinic.Common.Contracts.Patient;

public class GettingPatientDto : UserDto
{
    public Guid Id { get; set; }
    public string? Allergy { get; set; } = null!;
}
