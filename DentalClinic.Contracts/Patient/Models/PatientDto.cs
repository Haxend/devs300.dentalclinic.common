﻿using DentalClinic.Common.Contracts.User;

namespace DentalClinic.Common.Contracts.Patient;
public class PatientDto : UserDto
{
    public string? Allergy { get; set; } = null!;
}
