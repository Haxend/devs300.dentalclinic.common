﻿using DentalClinic.Common.Contracts.Common;

namespace DentalClinic.Common.Contracts.Patient
{
    public record PatientGetListRequest : RequestByPagination;
}
