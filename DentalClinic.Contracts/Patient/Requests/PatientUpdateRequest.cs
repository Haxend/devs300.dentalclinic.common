﻿namespace DentalClinic.Common.Contracts.Patient;

public class PatientUpdateRequest : PatientDto
{
    public Guid Id { get; set; }
}
