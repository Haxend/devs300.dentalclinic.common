﻿namespace DentalClinic.Common.Contracts.ServicePrice.Responses
{
    public class ServicePriceGetResponse : ServicePriceDto
    {
        public Guid Id { get; set; }
    }
}
