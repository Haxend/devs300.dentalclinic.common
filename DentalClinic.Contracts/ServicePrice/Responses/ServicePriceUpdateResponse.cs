﻿using DentalClinic.Common.Contracts.Common;

namespace DentalClinic.Common.Contracts.ServicePrice
{
    public class ServicePriceUpdateResponse : BaseResponse
    {
        public Guid Id { get; set; }
    }
}
