﻿using DentalClinic.Common.Contracts.Common;

namespace DentalClinic.Common.Contracts.ServicePrice
{
    public class ServicePriceDeleteResponse : BaseResponse
    {
        public Guid Id { get; set; }
    }
}
