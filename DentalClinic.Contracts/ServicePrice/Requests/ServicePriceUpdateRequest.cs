﻿namespace DentalClinic.Common.Contracts.ServicePrice
{
    public class ServicePriceUpdateRequest : ServicePriceDto
    {
        public Guid Id { get; set; }
    }
}
