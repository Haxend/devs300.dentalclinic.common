﻿namespace DentalClinic.Common.Contracts.ServicePrice
{
    public class ServicePriceDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal? Price { get; set; }
    }
}
