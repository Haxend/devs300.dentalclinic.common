﻿using DentalClinic.Common.Contracts.Common;

namespace DentalClinic.Common.Contracts.Doctor;

public class DoctorDeleteResponse : BaseResponse
{
    public Guid Id { get; set; }
}
