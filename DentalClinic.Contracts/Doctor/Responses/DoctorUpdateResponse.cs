﻿using DentalClinic.Common.Contracts.Common;

namespace DentalClinic.Common.Contracts.Doctor;

public class DoctorUpdateResponse : BaseResponse
{
    public Guid Id { get; set; }
}
