﻿namespace DentalClinic.Common.Contracts.Doctor;

public class DoctorGetResponse : DoctorDto
{
    public Guid Id { get; set; }
}
