﻿using DentalClinic.Common.Contracts.Common;

namespace DentalClinic.Common.Contracts.Doctor;

public class DoctorCreateResponse : BaseResponse
{
    public Guid Id { get; set; }
}
