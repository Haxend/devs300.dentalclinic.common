﻿using DentalClinic.Common.Contracts.User;

namespace DentalClinic.Common.Contracts.Doctor
{
    public class SyncDoctorCreate : UserDto
    {
        public Guid OuterId { get; set; }
        public string? Qualification { get; set; }
    }
}
