﻿using DentalClinic.Common.Contracts.User;

namespace DentalClinic.Common.Contracts.Doctor
{
    public class SyncDoctorUpdate : UserDto
    {
        public Guid OuterId { get; set; }
        public string? Qualification { get; set; }
    }
}
