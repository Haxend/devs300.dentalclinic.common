﻿using DentalClinic.Common.Contracts.User;

namespace DentalClinic.Common.Contracts.Doctor;

public class DoctorDto : UserDto
{
    public string? Qualification { get; set; }
}
