﻿namespace DentalClinic.Common.Contracts.Doctor;

public class DoctorShortDto
{
    public Guid Id { get; set; }
    public string Fio { get; set; }
}
