﻿using DentalClinic.Common.Contracts.User;

namespace DentalClinic.Common.Contracts.Doctor;

public class GettingDoctorDto : UserDto
{
    public Guid Id { get; set; }
    public string? Qualification { get; set; }
}
