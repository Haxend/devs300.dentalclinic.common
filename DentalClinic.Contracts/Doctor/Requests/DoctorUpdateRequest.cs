﻿namespace DentalClinic.Common.Contracts.Doctor;

public class DoctorUpdateRequest : DoctorDto 
{
    public Guid Id { get; set; }
}
