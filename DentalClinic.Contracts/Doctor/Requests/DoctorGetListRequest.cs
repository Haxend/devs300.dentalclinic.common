﻿using DentalClinic.Common.Contracts.Common;

namespace DentalClinic.Common.Contracts.Doctor
{
    public record DoctorGetListRequest : RequestByPagination;
}
