﻿using DentalClinic.Common.Contracts.Common;

namespace DentalClinic.Common.Contracts.Receptionist;

public class ReceptionistCreateResponse : BaseResponse
{
    public Guid Id { get; set; }
}
