﻿namespace DentalClinic.Common.Contracts.Receptionist;

public class ReceptionistGetResponse : ReceptionistDto
{
    public Guid Id { get; set; }
}
