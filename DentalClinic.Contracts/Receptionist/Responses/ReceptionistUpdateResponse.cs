﻿using DentalClinic.Common.Contracts.Common;

namespace DentalClinic.Common.Contracts.Receptionist;

public class ReceptionistUpdateResponse : BaseResponse
{
    public Guid Id { get; set; }
}
