﻿using DentalClinic.Common.Contracts.Common;

namespace DentalClinic.Common.Contracts.Receptionist;

public class ReceptionistDeleteResponse : BaseResponse
{
    public Guid Id { get; set; }
}
