﻿using DentalClinic.Common.Contracts.User;

namespace DentalClinic.Common.Contracts.Receptionist;

public class GettingReceptionistDto : UserDto
{
    public Guid Id { get; set; }
}
