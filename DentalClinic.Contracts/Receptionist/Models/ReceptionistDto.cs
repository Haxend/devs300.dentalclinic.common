﻿using DentalClinic.Common.Contracts.User;

namespace DentalClinic.Common.Contracts.Receptionist;

public class ReceptionistDto : UserDto;
