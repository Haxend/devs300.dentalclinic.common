﻿namespace DentalClinic.Common.Contracts.Receptionist;

public class ReceptionistUpdateRequest : ReceptionistDto
{
    public Guid Id { get; set; }
}
