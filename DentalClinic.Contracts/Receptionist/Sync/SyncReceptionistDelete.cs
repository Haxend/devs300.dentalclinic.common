﻿using DentalClinic.Common.Contracts.Common;

namespace DentalClinic.Common.Contracts.Receptionist
{
    public class SyncReceptionistDelete : RequestById;
}
