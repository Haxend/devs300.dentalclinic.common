﻿using DentalClinic.Common.Contracts.User;

namespace DentalClinic.Common.Contracts.Receptionist
{
    public class SyncReceptionistUpdate : UserDto
    {
        public Guid OuterId { get; set; }
    }
}
