﻿using DentalClinic.Common.Contracts.User;

namespace DentalClinic.Common.Contracts.Receptionist
{
    public class SyncReceptionistCreate : UserDto
    {
        public Guid OuterId { get; set; }
    }
}
