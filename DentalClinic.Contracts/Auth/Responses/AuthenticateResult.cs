﻿namespace DentalClinic.Common.DentalClinic.Contracts.Auth.Responses;

public record AuthenticateResult
{
    public string AccessToken { get; set; }
    public string RefreshToken { get; set; }
}