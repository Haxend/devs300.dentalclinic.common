﻿namespace DentalClinic.Common.Contracts.Auth.Requests;

public record Authenticate
{
    public string Email { get; set; }
    public string Password { get; set; }
}