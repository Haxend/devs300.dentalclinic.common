﻿namespace DentalClinic.Common.Contracts.OnlineAppointment.Responses
{
    public class OnlineAppointmentGetResponse : OnlineAppointmentDto
    {
        public Guid Id { get; set; }
    }
}
