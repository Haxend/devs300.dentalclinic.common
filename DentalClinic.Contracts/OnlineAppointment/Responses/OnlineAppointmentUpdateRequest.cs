﻿
using DentalClinic.Common.Contracts.Common;

namespace DentalClinic.Common.Contracts.OnlineAppointment.Responses
{
    public class OnlineAppointmentUpdateResponse : BaseResponse
    {
        public Guid Id { get; set; }
    }
}
