﻿
using DentalClinic.Common.Contracts.Common;

namespace DentalClinic.Common.Contracts.OnlineAppointment.Responses
{
    public class OnlineAppointmentDeleteResponse : BaseResponse
    {
        public Guid Id { get; set; }
    }
}
