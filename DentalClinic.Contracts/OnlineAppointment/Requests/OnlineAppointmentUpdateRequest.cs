﻿namespace DentalClinic.Common.Contracts.OnlineAppointment.Requests
{
    public class OnlineAppointmentUpdateRequest : OnlineAppointmentDto
    {
        public Guid Id { get; set; }
    }
}
