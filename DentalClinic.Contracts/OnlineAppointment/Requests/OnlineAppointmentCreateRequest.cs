﻿namespace DentalClinic.Common.Contracts.OnlineAppointment.Requests
{
    public class OnlineAppointmentCreateRequest : OnlineAppointmentDto;
}
