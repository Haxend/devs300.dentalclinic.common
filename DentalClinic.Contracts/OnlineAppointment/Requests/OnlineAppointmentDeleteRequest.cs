﻿
using DentalClinic.Common.Contracts.Common;

namespace DentalClinic.Common.Contracts.OnlineAppointment.Requests
{
    public class OnlineAppointmentDeleteRequest : RequestById;
}
