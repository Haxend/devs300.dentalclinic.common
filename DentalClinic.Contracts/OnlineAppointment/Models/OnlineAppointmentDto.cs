namespace DentalClinic.Common.Contracts.OnlineAppointment
{
    public class OnlineAppointmentDto
    {
        public DateTime? WantedDateVisit { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Email { get; set; }
        public string? Description { get; set; }
        public bool Approved { get; set; }
    }
}
