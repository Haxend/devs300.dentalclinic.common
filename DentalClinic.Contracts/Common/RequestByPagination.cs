﻿namespace DentalClinic.Common.Contracts.Common;

public abstract record RequestByPagination
{
    public int Offset { get; set; }
    public int Count { get; set; }
}