﻿namespace DentalClinic.Common.Contracts.Common;

public class BaseResponse
{        
    public bool Success { get; set; }
    public bool Failure
    {
        get
        {
            return !Success;
        }
    }
    public string ErrorMessage { get; set; }

    public static BaseResponse Ok()
    {
        return new BaseResponse {Success = true}; 
    }

    public static BaseResponse Error(string message)
    {
        return new BaseResponse { Success = false, ErrorMessage = message};
    }
}
