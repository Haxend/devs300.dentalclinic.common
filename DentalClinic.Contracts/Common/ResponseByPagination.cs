﻿namespace DentalClinic.Common.Contracts.Common;

public abstract record ResponseByPagination<T>
{
    public int TotalCount { get; set; }
    public List<T> Elements { get; set; } = [];
}