﻿namespace DentalClinic.Common.Contracts.Common;

public class RequestById
{
    public Guid Id { get; set; }
}