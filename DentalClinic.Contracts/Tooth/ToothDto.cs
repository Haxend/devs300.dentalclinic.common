﻿namespace DentalClinic.Application.Models.Tooth
{
    public class ToothDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int Code { get; set; }
    }
}