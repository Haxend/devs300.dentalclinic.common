﻿namespace DentalClinic.Common.Contracts.Notification;

public class NotificationRequest
{
   public string ToEmail { get; set; }
   public string Subject { get; set; }
   public string Body { get; set; }
}
