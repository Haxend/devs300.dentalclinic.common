﻿namespace DentalClinic.Common.Notifications.Models;

public record CreatedVisitNotification
{
    public string Email { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string FilialName { get; set; }
    public string Address { get; set; }
    public DateTime VisitDate { get; set; }
    public string From { get; set; }
    public string To { get; set; }
}