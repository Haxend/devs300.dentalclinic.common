﻿namespace DentalClinic.Common.DentalClinic.Contracts.Notification;

public record UserCreatedNotification
{
    public string Email { get; set; }
    public string Password { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
}