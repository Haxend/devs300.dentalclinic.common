﻿using System.ComponentModel.DataAnnotations;

namespace DentalClinic.Common.Contracts.User;

public class UserDto
{
    public string? FirstName { get; set; }

    [MaxLength(30, ErrorMessage = "Отчество не может быть таким длинным!")]
    public string? FatherName { get; set; }
    public string LastName { get; set; } = null;
    public string? Email { get; set; }
    public string? PasswordHash { get; set; }
    public string? PhoneNumber { get; set; }
    public Guid RoleId { get; set; }

}
